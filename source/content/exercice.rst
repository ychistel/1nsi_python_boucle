
Exercices
=========

.. exercice::

    #.  Donner dans chaque cas les valeurs affichées par la boucle ``for``.

        a.
            .. code:: python
                
                for m in range(5):
                    print(m)
        
        b. 
            .. code:: python
                
                for k in range(4,8):
                    print(k)
        
        c.
            .. code:: python
                
                for nombre in range(1,19,3):
                    print(nombre)
        
        d.
            .. code:: python
                
                for i in range(5):
                    print(i*10-1)

    #.  Écrire une boucle ``for`` pour afficher la suite de nombres dans chaque cas:

        a.  ``5, 6, 7, 8, 9``
        b.  ``0, 2, 4, 6, 8, 10, 12, 14, 16, 18``
        c.  ``-1, -2, -3, -4, -5``
        d.  ``4, 9, 16, 25, 36``

.. exercice::

    Les chaines de caractères sont itérables. Cela signifie qu'on peut parcourir un à un les caractères de la chaine avec une boucle ``for``

    #.  Créer une variable ``mot`` contenant la valeur ``numérique``.
    #.  Écrire une boucle ``for`` qui affiche chaque lettre du mot numérique:

        a.  En utilisant un indice ``i``.
        b.  Sans utiliser d'indice.

.. exercice::

    Soit ``t`` une variable de type tableau telle que ``t = [5,7,4,9,6,3,0,1,6]``.

    #.  Dans un éditeur Python, créer la variable ``t``.
    #.  Écrire un code qui modifient les valeurs du tableau ``t`` en multipliant chaque valeur par 3.
    #.  Écrire un code qui modifient les valeurs du tableau ``t`` en soustrayant 10 à chaque valeur.
    #.  Écrire un code qui modifient les valeurs du tableau ``t`` en élevant au carré chaque valeur.
    #.  Écrire un code qui remet à 0 chaque valeur de ``t``.


.. exercice::

    Soit ``t`` une variable qui contient le tableau telle que ``t = [5,7,4,9,6,3,0,1,6]``.

    #.  Dans un éditeur Python, créer la variable ``t``.
    #.  Écrire un code qui calcule la somme des valeurs du tableau ``t``.
    #.  Écrire un code qui calcule la valeur moyenne du tableau.

    .. Attention::

        On n'utilise pas la fonction ``sum``.

.. exercice::

    On donne les variables suivantes:

    .. code:: python

        jour = ("lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche")
        date = (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31)
        mois = ("janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre")

    #.  Écrire un code qui affiche les jours de la semaine.
    #.  Écrire un code qui affiche les mois de la semaine.
    #.  Écrire un code qui affiche les dates du mois de janvier en commençant par ``lundi 1 janvier`` jusqu'au 31 janvier.
    #.  a.  Écrire un code qui permet de saisir le numéro du mois, le nombre de jours contenus dans le mois, le premier jour du mois.
        b.  Compléter le code précédent pour afficher toutes les dates du mois saisi.