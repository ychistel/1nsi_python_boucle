La boucle bornée 
======================

Une **boucle** est une structure **itérative** et chaque répétition est une **itération**.

La boucle ``for`` est une boucle **bornée**. Elle répète **un nombre fini de fois** les instructions contenues dans la boucle.

La boucle ``for`` prélève dans l'ordre les valeurs présentes dans un ensemble **itérable** de valeurs.

Cet ensemble itérable de valeurs peut être:

-   un tableau ;
-   un n-uplet;
-   une chaine de caractère ;
-   une séquence de nombres entiers définie par ``range``.

Syntaxe de la boucle ``for``
----------------------------

.. code:: python

    for clé in ensemble_itérable_de_valeurs:
        instruction 1
        instruction 2
        etc
    # on désindente en fin de la boucle

La **clé** utilisée dans la boucle ``for`` est une **variable** qui prend une nouvelle valeur à chaque **itération**.

Par exemple, un tableau contient les valeurs "mot", 13, "deux", 6, 125 et "quatre" que l'on souhaite afficher.

.. pyscript::

    for cle in ["mot",13,"deux",6,125,"quatre"]:
        print(cle)

Itération avec RANGE
---------------------

Une variable qui contient un ``tableau`` ou ``n-uplet`` est **itérable**. Cela signifie que l'on peut parcourir ses éléments un à un.

Par exemple, si on veut afficher les 6 premiers nombres entiers non nuls, on peut écrire la boucle suivante:

.. pyscript::

    for i in [1,2,3,4,5,6]:
        print(i)

Seulement, cette méthode sera très longue si on veut afficher les 20 premiers nombres entiers !

En Python, l'instruction ``range`` construit une séquence **immuable** de nombres entiers régulièrement espacés. Cette instruction ``range`` s'utilise de la façon suivante:

-  ``range(n)`` renvoie la séquence de nombres entiers positifs de :math:`0` jusqu’à :math:`n-1`.
-  ``range(p,n)`` renvoie la séquence de nombres entiers positifs de :math:`p` jusqu’à :math:`n-1`.
-  ``range(p,n,k)`` renvoie la séquence de nombres entiers positifs de :math:`p`, :math:`p+k`, :math:`p+2k`, … jusqu'à :math:`n-1` au plus.

.. admonition:: Exemple

    - ``range(8)`` est la séquence de nombres entiers de 0 à 7 soit huit valeurs : 0, 1, 2, 3, 4, 5, 6, 7.
    - ``range(3,8)`` est la séquence de nombres entiers de 3 à 7 soit cinq valeurs : 3, 4, 5, 6, 7.
    - ``range(3,8,2)`` est la séquence de nombres entiers 3, 5, 7. Elle démarre à 3, progresse de 2 en 2 jusqu'à 8.

.. tip::

    L'exécution de ``range(3,8,2)`` dans l'interpréteur Python n'affiche pas les valeurs. Pour les afficher, on peut écrire une boucle ``for`` avec ``print``.

Pour afficher les 20 premiers nombres entiers, on détermine la séquence de nombres entiers avec range: ``range(1,21)``.

Ensuite, avec une boucle ``for`` on peut afficher nos nombres:

.. pyscript::

    for i in range(1,21):
        print(i)
