# Configuration file for the Sphinx documentation builder.

# # -- Path setup --------------------------------------------------------------f

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('./_extensions'))

# -- Project information -----------------------------------------------------

project = '1nsi_python_boucle'
author = 'Yannick Chistel'
version = '1.0'

# -- General configuration ---------------------------------------------------

extensions = ['numbered_title', 'sphinx_codemirror', 'sphinx_pyscript', 'nbsphinx', 'sphinx_copybutton']

templates_path = ['_templates']
exclude_patterns = ['nb']

# -- Options for HTML output -------------------------------------------------

html_theme = 'sphinx_book_theme'
html_theme_options = {'home_page_in_toc': True, 'logo': {'alt_text': 'Home', 'text': '1.NSI'}, 'use_download_button': True, 'icon_links': [{'name': 'GitLab', 'url': 'https://forge.apps.education.fr/ychistel/1nsi_python_boucle', 'icon': 'fa-brands fa-square-gitlab', 'type': 'fontawesome'}], 'primary_sidebar_end': ['sites.html']}

# Définir des variables pour les utiliser avec les fichiers placés dans les templates
html_context = {'sites': {'ENT Normandie': 'https://www.l-educdenormandie.fr/', 'PyData Sphinx Thème': 'https://pydata-sphinx-theme.readthedocs.io/en/stable/', 'Sphinx Book Thème': 'https://sphinx-book-theme.readthedocs.io/en/latest/index.html'}}

# Ajout du logo et du favicon pour le site
html_logo = '_static/img/logo.svg'
favicons = [
    "img/favicon-16x16.png",
    "img/favicon-32x32.png",
]
html_static_path = ['_static']
html_css_files = ['css/custom.css']
html_js_files = []
