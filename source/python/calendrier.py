jour = ("lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche")
date = (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31)
mois = ("janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre")

for m in mois:
    for i in range(31):
        print(f"{jour[i%7]} {date[i]} {m}")